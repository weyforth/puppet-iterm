class iterm {

	package { 'iTerm 2':
		ensure   => installed,
		provider => macapp,
		source   => 'https://iterm2.com/downloads/stable/iTerm2_v2_0.zip',
	}

}
